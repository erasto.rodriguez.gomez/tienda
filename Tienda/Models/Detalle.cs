﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Tienda.Models
{
    class Detalle
    {
        public int Id { get; set; }
        public int ProductoId { get; set; }
        public int VentaId { get; set; }
        public Venta Venta { get; set; }
        public int Cantidad { get; set; }
        public double Subtotal { get; set; }
        public virtual ICollection<Detalle> Detalles { get; set; }




    }
}
