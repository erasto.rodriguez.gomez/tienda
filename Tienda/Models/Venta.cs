﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.Models
{
    class Venta
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Cliente { get; set; }
        public double Total { get; set; }


    }
}
