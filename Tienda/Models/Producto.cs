﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.Models
{
    class Producto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Categoria { get; set; }
        public double Precio { get; set; }

        public override string ToString()
        {

            return $"{Id})Nombre:{Nombre}\n Precio: {Precio} Pesos\n Desc: {Descripcion}";
        }
    }
}
