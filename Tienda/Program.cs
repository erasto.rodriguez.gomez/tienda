﻿using System;
using System.Linq;
using Tienda.Models;

namespace Tienda
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Menu();
        }

        public static void Menu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1) Buscar Pelicula");
            Console.WriteLine("2) Alta Pelicula");
            Console.WriteLine("3) Actualiza Pelicula");
            Console.WriteLine("4) Eliminar Pelicula");
            Console.WriteLine("0) Salir");

            string opcion = Console.ReadLine();
            switch (opcion)
            {

                case "1": BuscarPelicula();
                    break;
                case "2": AltaPelicula();
                    break;
                case "3": ActualizarPelicula();
                    break;
                case "4": EliminarPelicula();
                    break;
                case "0": return;
            }


            Menu();
        }

        public static void BuscarPelicula()
        {
            Console.WriteLine("Buscar Pelicula");
            Console.Write("Buscar: ");
            string buscar = Console.ReadLine();

            using (TiendaContext context = new TiendaContext())
            {
                IQueryable<Producto> productos = context.productos.Where(p => p.Nombre.Contains(buscar));
                foreach (Producto producto in productos)
                {
                    Console.WriteLine(producto);
                }
            }
        }
        public static void AltaPelicula()

        {

            Console.WriteLine("Alta Pelicula");
            Producto producto = new Producto();
            producto = ModificarProducto(producto);

            

            using (TiendaContext context = new TiendaContext())

            {
                context.Add(producto);
                context.SaveChanges();
                Console.WriteLine("Pelicula dada de alta");
            }



        }


           public static Producto ModificarProducto(Producto producto)
        {
            Console.WriteLine("Nombre:");
            producto.Nombre = Console.ReadLine();
            Console.WriteLine("Descripcion:");
            producto.Descripcion = Console.ReadLine();
            Console.WriteLine("Categoria:");
            producto.Categoria = Console.ReadLine();
            Console.WriteLine("Precio:");
            producto.Precio = double.Parse(Console.ReadLine());
            return producto;
            
        }

        public static Producto SeleccionarProducto()
        {
            BuscarPelicula();
            Console.Write("Selecciona el numero de Pelicula");
            int id = int.Parse(Console.ReadLine());
            using (TiendaContext context = new TiendaContext())

            {
                Producto producto = context.productos.Find(id);
                if (producto == null)
                {
                    SeleccionarProducto();
                }

                return producto;
            }

        }
        
        public static void ActualizarPelicula()
        {
            Console.WriteLine("Actualizar Pelicula");
            Producto producto = SeleccionarProducto();
            producto = ModificarProducto(producto);
;
            using (TiendaContext context = new TiendaContext())

            {
                context.Update(producto);
                context.SaveChanges();
                Console.WriteLine("Pelicula actualizada");
            }
        }

        public static void EliminarPelicula()
        {
            Console.WriteLine("Eliminar Producto");
            Producto producto = SeleccionarProducto();
            using (TiendaContext context = new TiendaContext())

            {
                context.Remove(producto);
                context.SaveChanges();
                Console.WriteLine("Pelicula Eliminada");
            }
        }


     }   
}


                





  
    




